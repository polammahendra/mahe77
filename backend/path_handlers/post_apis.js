import { validate_login_data } from "../utils/authentication_validation.js";
import { EMPLOYEE_DETAILS_DATA } from "../utils/models.js";

/**
 * @path /login is the path by using this path we can post the user login data and validate it using the register data
 * @param request is the callback parameter that contains the data that passed from client(postman)
 * based on that data server respond
 * @param response is the callback parameter that sends the data based on the request data
 */

const login_employee_handler = (request, response) => {
  // login_data is the variable that contains the request body data
  const login_data = request.body;
  // initializing the count is 0 because we need to validate the request parameters
  var count = 0;
  // iteraing over the body data because we need to count the body params
  for (let item in login_data) {
    // updating the count
    count = count + 1;
  }
  // geting user_type from request params
  const employee_type = request.params.employee_type;
  /*
      we need only username and password for user login
              if any extra params are pass in the request it throws error */
  if (employee_type === "employee") {
    validate_login_data(login_data, response);
  }
};

const employee_details = [];
const post_the_data = (request, response) => {
  const name = request.body.name;
  const empid = request.body.empid;
  const empsalary = request.body.empsalary;
  const attendance = request.body.attendance;
  const yearofjoining = request.body.yearofjoining;
  const role = request.body.role;
  const email = request.body.email;
  const password = request.body.password;

  const post_data = {
    name: name,
    empid: empid,
    empsalary: empsalary,
    attendance: attendance,
    yearofjoining: yearofjoining,
    role: role,
    email: email,
    password: password,
  };
  const posts_data = new EMPLOYEE_DETAILS_DATA(post_data);
  posts_data.save().then(() => response.send("data saved"));
  employee_details.push(posts_data);
};

export { login_employee_handler, post_the_data };

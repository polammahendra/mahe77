api 1 
method : POST
path : /login/:role 
body:{role,empid,password}
Description : based on role respective home page will open

api 2
method : GET
path : /emplist/:role
Description : gets employee list based on the role


api 3
method : get
path : /emplist/:empid
Description : gets employee details from the employee list



api 4
method : POST
path : /emplist/add_emp
body : {name, empid, yearofjoining, role, email, phoneno, password}
Description : admin can add employee details in add_emp page.

api 5
method : POST
path : /timesheets
body : {date, hours, empid, dayspresent, daysabsent, empsalary}
Description : employee can post timesheets

api 6
method : POST
path : /timesheets/taskPage
body : {taskname, hours, description}
Description : employee can post Tasks 



import { EMPLOYEE_LOGIN_DATA } from "./models.js";
import { inserting_employee_login_data_into_database } from "./models.js";

/**
 * validate the user login data based on the user register database data
 * @param login_data that contains the user login data
 * @param response which is the response object that send the response to the client
 * @returns the different response like if username not found or password not correct or login success
 * based on the conditions with validating the user register database data
 */
function validate_login_data(login_data, response) {
  // fetching the user login data
  EMPLOYEE_LOGIN_DATA.find({ empid: `${login_data.empid}` }, (err, data) => {
    if (err) {
      console.log(err);
    } else {
      if (data.length === 0) {
        // fetching the user register data from the data base for login validatinn
        EMPLOYEE_LOGIN_DATA.find({ empid: login_data.empid }, (err, data) => {
          // any error occur in the data fetching it prints in the console
          if (err) {
            // printing error in console
            console.log(err);
            //if there is no error in the data fetching it goes through the else block
          } else {
            // if there any data that matches with the username it goes through the if condition
            if (data.length !== 0) {
              // iterating over the data fetched from the database
              for (let item of data) {
                // validating the password
                if (item.password === login_data.password) {
                  login_data.status_code = 200;
                  //const jwt_token = jwt_token_generator(login_data.empid);
                  //login_data.jwt_token = jwt_token;
                  inserting_employee_login_data_into_database(login_data);
                  response.send(JSON.stringify(login_data));
                  break;
                }
                // if password is invalid it sends the response as password not match
                else {
                  const obj = {
                    status_code: 401,
                    status_message: "invalid password",
                  };
                  response.send(JSON.stringify(obj));
                  break;
                }
              }
            }
            // if fetching data count is empty it sends the response as user not found
            else {
              const obj = {
                status_code: 404,
                status_message: "empid not found",
              };
              response.send(JSON.stringify(obj));
            }
          }
        });
      } else {
        var message = employee_alredy_login(login_data.username);
        response.send(JSON.stringify(message));
      }
    }
  });
}

/**
 *
 * @param {1} username it caontains username that are trying to login
 * @returns a object that contains status_code and status_message
 */
function employee_alredy_login(empid) {
  return {
    status_code: 406,
    status_message: `Already logged in as ${empid}`,
  };
}

export { validate_login_data, employee_alredy_login };

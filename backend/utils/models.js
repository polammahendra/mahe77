// importing mongoose from mongoose
import mongoose from "mongoose";

import { User_Schema } from "./schema.js";
import { UserLogin_Schema } from "./schema.js";
import { TaskDetailsSchema } from "./schema.js";
import { TimesheetsSchema } from "./schema.js";

// connecting the db
mongoose.connect(
  "mongodb+srv://arunchand777:Arunchand@cluster0.ucw6a.mongodb.net/time_sheets?retryWrites=true&w=majority"
);

// creating a model instance for the user for creating a list and enter objects in it
const UserModel = mongoose.model("UserModel", User_Schema);

// creating a model instance for the UserLogin_Model for storing user login details
const UserLogin_Model = mongoose.model("UserLogin_Model", UserLogin_Schema);

// creating a model instance for the TaskDetails_Model for storing taskdetails
const TaskDetails_Model = mongoose.model(
  "TaskDetails_Model",
  TaskDetailsSchema
);

// creating a model instance for the Timesheet_Model for storing timesheet details
const Timesheet_Model = mongoose.model("Timesheet_Model", TimesheetsSchema);

/**
 * Inserting the user registering data into the database
 * @param register_data database contains the user registered data
 */
function inserting_register_data_into_database(register_data) {
  const employee_data = new UserModel({
    name: register_data.name,
    empid: register_data.empid,
    yearofjoining: register_data.yearofjoining,
    role: register_data.role,
    email: register_data.email,
    phone_no: register_data.phone_no,
    password: register_data.password,
  });
  // saving the data
  employee_data
    .save()
    .then(() => console.log("data saved"))
    .catch((err) => {
      console.log(err);
    });
}

export {
  UserModel,
  UserLogin_Model,
  TaskDetails_Model,
  Timesheet_Model,
  inserting_register_data_into_database,
};

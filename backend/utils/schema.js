// importing mongoose from mongoose
import mongoose from "mongoose";

/**
 * This is the definition of the schema to be used to convert into a model
 * for the MongoDB implementation.
 * This model represents a User on the application.
 */
const User_Schema = new mongoose.Schema({
    name:             { type: String, required: true},
    empid:            { type: Number, required: true, unique: true},
    password:         { type: String, required: true },
    yearofjoining:    { type: Number, required: true },
    role:             { type: String, required: true, default: "employee" },
    email:            { type: String, required: true, unique: true },
    phone_no:         { type: Number, required: true}
})

/**
 * This is the definition of the schema to be used to convert into a model
 * for the MongoDB implementation.
 * This model represents a UserLogin on the application.
 */
const UserLogin_Schema = new mongoose.Schema({
    role:             {type: String, required: true, default: "employee"},
    empid:            { type: Number, required: true, unique: true},
    password:         {type: Number, required: true},
});

/**
 * This is the definition of the schema to be used to convert into a model
 * for the MongoDB implementation.
 * This model represents Tasks on the application.
 */
const TaskDetailsSchema = new mongoose.Schema({
    taskname:         {type: String, required: true},
    hours:            { type: Number, required:true},
    description:      {type: String, required: true},
});

/**
 * This is the definition of the schema to be used to convert into a model
 * for the MongoDB implementation.
 * This model represents Timesheets on the application.
 */
const TimesheetsSchema = new mongoose.Schema({
    date:             { type:Date, required: true },
    hours:            { type: Number, required: true },
    empid:            { type: Number, required: true },
    dayspresent:      { type: Number, required: true },
    daysabsent:       { type: Number, required: true },
    empsalary:        { type: Number, required: true },
});

export {
  User_Schema,
  UserLogin_Schema,
  TaskDetailsSchema,
  TimesheetsSchema,
};

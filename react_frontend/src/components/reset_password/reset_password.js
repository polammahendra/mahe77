import React from "react";
import "./reset_password.css"
const Reset = ()=>{
    return(
        <div className="mainDiv">
        <div className="cardStyle">
            <form action="" method="post" name="signupForm" id="signupForm">
                <h2 className="formTitle">
                    Reset your password
                </h2>

                <div className="inputDiv">
                    <label class="inputLabel" for="password">New Password</label>
                    <input type="password" id="password" name="password" required/>
                </div>

                <div className="inputDiv">
                    <label className="inputLabel" for="confirmPassword">Confirm Password</label>
                    <input type="password" id="confirmPassword" name="confirmPassword"/>
                </div>

                <div className="buttonWrapper">
                    <button type="submit" id="submitButton" onclick="window.open('loginpage.html')"
                        className="submitButton pure-button pure-button-primary">
                        <span>Continue</span>
                    </button>
                </div>

            </form>
        </div>
    </div>
    )
}

export default Reset;
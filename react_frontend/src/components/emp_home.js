import React from "react";
import "./emp_home.css";

const EmployeeHome = () => {
  return (
    <div class="container">
      <div class="header">
        <a href="#" class="logo">
          Employee Profile
        </a>
        <div class="header-right">
          <a href="#logout">Logout</a>
        </div>
      </div>
      <div>
        <table>
          <tr>
            <td>
              <section>
                <label for="fileToUpload">
                  <i class="fa fa-camera"></i>
                  {/* <input type="file" id="fileToUpload" /> */}
                </label>
                <img
                  src="https://i.ibb.co/yNGW4gg/avatar.png"
                  id="blah"
                  alt="Avatar"
                />
              </section>
              <h1>James Bond</h1>
              <h3>FullStack Developer</h3>
            </td>
            <td>
              <ul>
                <li>
                  <b>Full name: </b>jamesbond{" "}
                </li>
                <li>
                  <b>Emp-ID: </b> 007{" "}
                </li>
                <li>
                  <b>Blood group: </b> O+ve{" "}
                </li>
                <li>
                  <b>Email: </b> jjj@gmail.com{" "}
                </li>
                <li>
                  <b>Contact number: </b>77777
                </li>
                <li>
                  <b>Address: </b>wertygfcgh
                </li>
              </ul>
            </td>
          </tr>
          <div class="timesheets">
            <h3>Update your timesheet here:</h3>
            <button>Timesheets</button>
          </div>
        </table>
      </div>
    </div>
  );
};

export default EmployeeHome;

import React from "react";
import "./hrHomePage.css";
import { Link } from "react-router-dom";

const HrHomePage = ()=>{
    return (
      <div class="container">
        <div class="header">
          <a href="#" class="logo">
            HR Profile
          </a>
          <div class="header-right">
            <Link to="/">
              <a href="#logout">Logout</a>
            </Link>
          </div>
        </div>
        <div>
          <table>
            <tr>
              <td>
                <section>
                  <label for="fileToUpload">
                    <i class="fa fa-camera"></i>
                    {/* <input type="file" id="fileToUpload" style="visibility:hidden;" accept=".png,.jpg,jpeg,.PNG,.JPEG" name="fileToUpload" onchange="document.getElementById('blah').src = window.URL.createObjectURL(this.files[0])"> */}
                  </label>
                  <img
                    src="https://i.ibb.co/yNGW4gg/avatar.png"
                    id="blah"
                    alt="Avatar"
                  />
                </section>
                <h1>James Bond</h1>
                <h3>HR</h3>
              </td>
              <td>
                <ul>
                  <li>
                    <b>Full name: </b>jamesbond{" "}
                  </li>
                  <li>
                    <b>Emp-ID: </b> 007{" "}
                  </li>
                  <li>
                    <b>Blood group: </b> O+ve{" "}
                  </li>
                  <li>
                    <b>Email: </b> jjj@gmail.com{" "}
                  </li>
                  <li>
                    <b>Contact number: </b>77777
                  </li>
                  <li>
                    <b>Address: </b>wertygfcgh
                  </li>
                </ul>
              </td>
            </tr>
            <div class="timesheets">
              <h3>Update your timesheet here:</h3>
              <button>Timesheets</button>
            </div>
            <div class="emplist">
              <h3>Click here for Employee List:</h3>
              <Link to="/emplisthr">
                <button>Employee List</button>
              </Link>
            </div>
          </table>
        </div>
      </div>
    );
}
export default HrHomePage;
import React from "react";
import { Link } from "react-router-dom";
import "./adminHomePage.css";

const AdminHomePage = () => {
  return (
    <div className="container">
      <div className="header">
        <a href="#" className="logo">
          Admin Profile
        </a>
        <div className="header-right">
          <Link to="/">
            <a href="#logout">Logout</a>
          </Link>
        </div>
      </div>
      <div>
        <table>
          <tr>
            <td>
              <section>
                <label for="fileToUpload">
                  <i className="fa fa-camera"></i>
                  {/* <input type="file" id="fileToUpload" /> */}
                </label>
                <img
                  src="https://i.ibb.co/yNGW4gg/avatar.png"
                  id="blah"
                  alt="Avatar"
                />
              </section>
              <h1>James Bond</h1>
              <h3>Admin</h3>
            </td>
            <td>
              <ul>
                <li>
                  <b>Full name: </b>jamesbond{" "}
                </li>
                <li>
                  <b>Emp-ID: </b> 007{" "}
                </li>
                <li>
                  <b>Blood group: </b> O+ve{" "}
                </li>
                <li>
                  <b>Email: </b> jjj@gmail.com{" "}
                </li>
                <li>
                  <b>Contact number: </b>77777
                </li>
                <li>
                  <b>Address: </b>wertygfcgh
                </li>
              </ul>
            </td>
          </tr>
          <div className="timesheets">
            <h3>Update your timesheet here:</h3>
            <button>Timesheets</button>
          </div>
          <div className="emplist">
            <h3>Click here for Employee List:</h3>
            <button>Employee List</button>
          </div>
        </table>
      </div>
    </div>
  );
};

export default AdminHomePage;

import React from "react";
import { Link } from "react-router-dom";
import "./empSalAttendance.css";

const EmpSalAttendance = () => {
  return (
    <div className="container">
      <header role="banner">
        <h1>HR panel</h1>
        <ul className="utilities">
          <br />
          <li className="users">
            <a href="#">My Account</a>
          </li>
          <li className="logout warn">
            <a href="">Log Out</a>
          </li>
        </ul>
      </header>

      <main role="main">
        <section>
          <br />
          <h1>Employee Details</h1>
          <div className="tbl-header">
            <table cellpadding="10" cellspacing="10" border="0">
              <thead>
                <tr>
                  <th>S.No</th>
                  <th>Emp_Id</th>
                  <th>Emp_name</th>
                  <th>Emp_attendence</th>
                  <th>Emp_salary</th>
                </tr>
              </thead>
            </table>
          </div>
          <div className="tbl-content">
            <table cellpadding="0" cellspacing="0" border="0">
              <body>
                <tr>
                  <td>1</td>
                  <td>107xx</td>
                  <td>Arun</td>
                  <td>abcd</td>
                  <td>25000</td>
                </tr>
                <tr>
                  <td>2</td>
                  <td>106xx</td>
                  <td>Hari</td>
                  <td>efgh</td>
                  <td>2600</td>
                </tr>
                <tr>
                  <td>3</td>
                  <td>105xx</td>
                  <td>Reddy</td>
                  <td>jndi</td>
                  <td>2700</td>
                </tr>
              </body>
            </table>
          </div>
        </section>

        <input type="button" value="Download" className="btn" />
      </main>
    </div>
  );
};

export default EmpSalAttendance;
